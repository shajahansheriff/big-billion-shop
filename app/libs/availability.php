<?php
class Availability {

	public static function display($availability){

		if ($availability == 0 ) {
			echo "Out Of Stock";
		}
		elseif ($availability == 1) {
			echo "In Stock";
		}
	} 

	public static function displayClass($availability) {
		if ($availability == 0) {
			echo "close";
		}
		elseif ($availability == 1) {
			echo "string";
		}
	}
}