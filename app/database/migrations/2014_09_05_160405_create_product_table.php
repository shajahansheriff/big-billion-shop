<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Products', function($table){
			$table->Bigincrements('id');
			$table->string('title');
			$table->integer('category_id');
			$table->foreign('category_id')->reference('id')->on('categories');
			$table->text('Description');
			$table->decimal('price',4,2);
			$table->boolean('availability')->default(1);
			$table->string('image');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Products');
	}

}
