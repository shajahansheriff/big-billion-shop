<?php

class UserTableSeeder extends Seeder{

	public function run(){
		$user = new User;
		$user->fname = 'Shajan';
		$user->lname = 'Sheriff';
		$user->email = 'shajansheriff@gmail.com';
		$user->password = Hash::make('mypassword');
		$user->admin = 1;
		$user->save();
	}
}