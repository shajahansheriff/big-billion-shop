<?php 

class Product extends Eloquent {

	protected $fillable = array('title','description','image','price','category_id','availability');
	public static $rules = array('producttitle'=>'required|min:3',
								'price'=>'required|numeric',
								'description'=>'required|min:20',
								'category_id'=>'required|integer',
								'availability'=>'integer',
								'image'=>'required|image|mimes:jpeg,jpg,png,gif'
								);

	public function category()
	{
		return $this->belongsTo('Category');
	}


}