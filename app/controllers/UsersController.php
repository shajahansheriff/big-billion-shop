<?php

class UsersController extends Basecontroller{

	public function __construct()
	{
		parent::__construct();	
		$this->beforeFilter('csrf',array('on'=>'post'));
	}
	public function getSignup(){
		return View::make('user.signup');
	}

	public function postSignup(){
		$validator = Validator::make(Input::all(),User::$rules);

		if ($validator->passes()) {
			$user = new User;
			$user->fname = Input::get('fname');
			$user->lname = Input::get('lname');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->save();

			return Redirect::to('user/signin')
							->with('message','Account Created Successfully. Now Please Signin to continue.');
		}
		return Redirect::to('user/signup')
						->with('message','Account Not Created Successfully. Try Again')
						->withErrors($validator)
						->withInput();
	}

	public function getSignin(){
		if(Auth::check())
			return Redirect::intended();
		return View::make('user.signin');
	}

	public function postSignin(){
		$email = Input::get('email');
		$password = Input::get('password');
		if(Auth::attempt(array('email'=> $email,'password'=>$password)))
			{
				return Redirect::intended()
					->with('message','Successfully Logged In');
			}
		return Redirect::to('user/signin')
					->with('message','incorrect Credientials');
	}

	public function getSignout(){
		Auth::logout();
		return Redirect::to('user/signin')
						->with('message','You have been logged out');
	}

}