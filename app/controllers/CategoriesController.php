<?php
class CategoriesController extends BaseController {

	public function __construct()
	{
		parent::__construct();	
		$this->beforeFilter('csrf',array('on'=>'post'));
		$this->beforeFilter('admin');

	}

	public function getIndex()
	{
		return View::make('admin.category')
				->with('categories',Category::all());
	}

	public function postCreate()
	{

		$validator = Validator::make(Input::all(), Category::$rules);
		if ($validator->passes())
		{	
			$category = new Category;
			$category->title =  Input::get('cname');
			$category->save();

			return Redirect::to('admin/category')
					->with('message','Category Created Successfully');
		}
		return Redirect::to('admin/category')
				->with('message','Category not create Successfully')
				->withErrors($validator)
				->withInput();

	}

	public function postDestroy()
	{
		$category = Category::find(Input::get('id'));

		if ($category) {
			$category->delete();

			return Redirect::to('admin/category')
					->with('message','Category Deleted Successfully');
		}

		return Redirect::to('admin/category')
					->with('message','Category Not Deleted Successfully');
	}

}