<?php 

class ProductsController extends BaseController {

	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf',array('on'=>'post'));
		$this->beforeFilter('admin');
	}

	public function getIndex()
	{
		$categories = array();
		foreach (Category::all() as $category) {
			$categories[$category->id] = $category->title;
		}
		return View::make('admin.product')
				->with('categories',$categories)
				->with('products',Product::all());
	}

	public function postCreate()
	{
		$validator = Validator::make(Input::all(),Product::$rules);

		if ($validator->passes()) 
		{
			$product = new Product;
			$product->title = Input::get('producttitle');
			$product->description = Input::get('description');
			$product->price = Input::get('price');
			$product->category_id = Input::get('category_id');
			$image = Input::file('image');
			$image2 = Input::file('image2');
			$image3 = Input::file('image3');
			$image4 = Input::file('image4');
			$filename = date('y-m-d').'-'.$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(500,500)->save('public/assets/products/'.$filename);
			$product->image = 'assets/products/'.$filename;
			
			$filename2 = date('y-m-d').'-'.$image2->getClientOriginalName();
			Image::make($image2->getRealPath())->resize(500,500)->save('public/assets/products/'.$filename2);
			$product->image2 = 'assets/products/'.$filename2;
			
			$filename3 = date('y-m-d').'-'.$image3->getClientOriginalName();
			Image::make($image3->getRealPath())->resize(500,500)->save('public/assets/products/'.$filename3);
			$product->image3 = 'assets/products/'.$filename3;
			
			$filename4 = date('y-m-d').'-'.$image4->getClientOriginalName();
			Image::make($image4->getRealPath())->resize(500,500)->save('public/assets/products/'.$filename4);
			$product->image4 = 'assets/products/'.$filename4;
			
			$product->save();

			return Redirect::to('admin/product')
							->with('message','Product Added Successfully');
		}

		return Redirect::to('admin/product')
						->with('message','Product Not Successfully Added')
						->withErrors($validator)
						->withInput();
	}

	public function postDestroy()
	{
		$product = Product::find(Input::get('product_id'));

		if ($product) {
			File::delete('public/assets/products'.$product->image);
			$product->delete();

			return Redirect::to('admin/product')
							->with('message','Product Delted Successfully');
		}

		return Redirect::to('admin/product')
						->with('message','Product Not Delted Successfully');
	}

	public function postToggleAvailabilty(){
		$product = Product::find(Input::get('product_id'));
		if ($product) {
			$product->availability = Input::get('availability');
			$product->save();

			return Redirect::to('admin/product')
							->with('message','Product Availabilty Updated');
		}

		return Redirect::to('admin/product')
						->with('message','Product Availabilty Not Successfully Updated');
		

	}
}