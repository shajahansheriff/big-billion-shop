<?php

class StoreController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		parent::__construct();	
		$this->beforeFilter('csrf',array('on'=>'post'));
		$this->beforeFilter('auth', array('only'=>array(
			'postAddtocart','getCart','getRemoveItem','placeorder'
			)));
	}
	public function getIndex()
	{
		return View::make('index')
					->with('new_products',Product::take(6)->orderBy('created_at','DESC')->get())
					->with('popular_products',Product::take(4)->orderBy('visitCount','DESC')->get())
					->with('page','index');

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function getProduct($id)
	{
		$product = Product::find($id);
		$product->visitCount = $product->visitCount + 1;
		$product->save();
		$category = Category::find($product->category_id);
		return View::make('store.product')
					->with('product',$product)
					->with('category',$category);

	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function getCategory($cat_id)
	{
		return View::make('store.category')
					->with('products',Product::where('category_id','=',$cat_id)->get())
					->with('category',Category::find($cat_id));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getSearch()
	{
		$keyword = Input::get('keyword');

		return View::make('store.search')
					->with('products',Product::where('title','LIKE','%'.$keyword.'%')->get())
					->with('keyword',$keyword);
	}

	public function postAddtocart(){
		$product = Product::find(Input::get('id'));
		$quantity = Input::get('qty');

		Cart::insert(array(
			'id'=>$product->id,
			'name'=>$product->title,
			'quantity'=>$quantity,
			'price'=>$product->price,
			'image'=>$product->image
			));
		return Redirect::to('store/cart');
	}

	public function getCart(){
		return View::make('store.cart')->with('cart_products', Cart::contents());
	}

	public function getRemoveItem($identifier){
		$item = Cart::item($identifier);
		$item->remove();

		return Redirect::to('store/cart');

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getContact()
	{
		return View::make('store.contact');
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getTest()
	{
		return var_dump(View::make('index'));
	}


}
