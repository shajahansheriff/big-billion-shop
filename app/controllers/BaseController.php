<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */

	public function __construct(){
		$this->beforeFilter(function(){
			View::share('catnav',Category::all());
			View::composer('*',function($view){
				View::share('page',$view->getName());
			});
		}); 
	}
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
