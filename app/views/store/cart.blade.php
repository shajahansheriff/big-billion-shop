@extends('layouts.main')

@section('content')


	<div class="span9">
    <ul class="breadcrumb">
		<li><a href="index.html">Home</a> <span class="divider">/</span></li>
		<li class="active"> SHOPPING CART</li>
    </ul>
	<h3>  SHOPPING CART [ <small>3 Item(s) </small>]<a href="products.html" class="btn btn-large pull-right"><i class="icon-arrow-left"></i> Continue Shopping </a></h3>	
	<hr class="soft"/>
	@if( !Auth::user() ) 

	<table class="table table-bordered">
	<tbody>
		<tr><th colspan="2"> I AM ALREADY REGISTERED  </th></tr>
		 <tr> 
		 <td colspan="2">
		 	{{ Form::open(array('url'=>'user/signin','class'=>'form-horizontal')) }}
			
			  <div class="control-group">
				<label class="span2 control-label" for="username">Username</label>
				<div class="controls">
					{{ Form::input('email','email')}}
				  
				</div>
			  </div>
			 </div>
			  <div class="control-group">
				<label class="span2 control-label" for="inputPassword">Password</label> 
				<div class="controls">
				  {{ Form::password('password',null)}} <a href="forgetpass.html">Forgot your password?</a>
				</div>
			  </div>
			  <div class="control-group">
				<div class="controls">
				  <button type="submit" class="btn">Sign in</button> (or) {{ HTML::link('user/signup','Register now !') }}  </a>
				</div>
			  </div>
			{{ Form::close() }}			  
		  </td>
		  </tr>
	  </tbody>
	</table>	
	@endif	
			
	<table class="table table-bordered">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Description</th>
                  <th>Quantity/Update</th>
				  <th>Price</th>
                  
				</tr>
              </thead>
              <tbody>
              	@foreach($cart_products as $cart_product )
                <tr>
                  <td> {{ HTML::image($cart_product->image, $cart_product->name, array('width'=>'60')) }}</td>
                  <td>{{ $cart_product->name }}<br/>Color : black, Material : metal</td>
				  <td>
					{{ $cart_product->quantity }}				</div>
				  </td>
                  <td>{{ $cart_product->price }}</td>
                  
                </tr>
				@endforeach
				
                <tr>
                  <td colspan="6" align="right">Total Price:	</td>
                  <td>{{ Cart::total()}}</td>
                </tr>
				 <tr>
                  <td colspan="6" align="right">Total Discount:	</td>
                  <td> 0 </td>
                </tr>
                 <tr>
                  <td colspan="6" align="right">Total Tax:	</td>
                  <td> 0 </td>
                </tr>
				 <tr>
                  <td colspan="6" align="right"><strong>TOTAL ({{ Cart::total() }} - 0 + 0) =</strong>	</td>
                  <td class="label label-important"> <strong> {{ Cart::total() }} </strong></td>
                </tr>
				</tbody>
            </table>
		
		
            <table class="table table-bordered">
			<tbody>
				 <tr>
                  <td> 
				<form class="form-inline">
				<label style="min-width:159px"><strong> VOUCHERS CODE:</strong> </label> 
				<input type="text" class="input-medium" placeholder="CODE">
				<button type="submit" class="btn"> ADD</button>
				</form>
				</td>
                </tr>
				
			</tbody>
			</table>
			
			<table class="table table-bordered">
			<tbody>
                <tr><th colspan="2">ESTIMATE YOUR SHIPPING </th></tr>
                 <tr> 
				 <td>
					<form class="form-horizontal">
					  <div class="control-group">
						<label class="span2 control-label" for="inputCountry">Country</label>
						<div class="controls">
						  <input type="text" id="inputCountry" placeholder="Country">
						</div>
					  </div>
					  <div class="control-group">
						<label class="span2 control-label" for="inputPost">Post Code/ Zipcode</label>
						<div class="controls">
						  <input type="text" id="inputPost" placeholder="Postcode">
						</div>
					  </div>
					  <div class="control-group">
						<div class="controls">
						  <button type="submit" class="btn">ESTIMATE</button>
						</div>
					  </div>
					</form>				  
				  </td>
				  </tr>
              </tbody>
            </table>		
	<a href="products.html" class="btn btn-large"><i class="icon-arrow-left"></i> Continue Shopping </a>
	<a href="login.html" class="btn btn-large pull-right">Next <i class="icon-arrow-right"></i></a>
	
</div>


@stop