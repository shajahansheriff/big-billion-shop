@extends('layouts.main')

@section('content')

	<div class="span9">
    <ul class="breadcrumb">
		<li><a href="index.html">Home</a> <span class="divider">/</span></li>
		<li class="active">Login</li>
    </ul>
	<h3> Login</h3>	
	<hr class="soft"/>
	@if (Session::has('message'))
		
			<div class="alert alert-block alert-error fade in">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{{ Session::get('message') }}
	 		</div>
		
		@endif

		@if ($errors->has()) 
		<ul>
			@foreach ($errors->all() as $error) 
			<li>
				{{$error}}
			</li>
			@endforeach
		</ul>
		@endif
	<div class="row">
		<div class="span4">
			<div class="well">
			<h5>CREATE YOUR ACCOUNT</h5><br/>
			Enter your e-mail address to create an account.<br/><br/><br/>
			<form action="register.html">
			  <div class="control-group">
				<label class="control-label" for="inputEmail">E-mail address</label>
				<div class="controls">
				  <input class="span3"  type="text" id="inputEmail" placeholder="Email">
				</div>
			  </div>
			  <div class="controls">
			  <button type="submit" class="btn block">Create Your Account</button>
			  </div>
			</form>
		</div>
		</div>
		<div class="span1"> &nbsp;</div>
		<div class="span4">
			<div class="well">
			<h5>ALREADY REGISTERED ?</h5>
			{{ Form::open(array('url'=>'user/signin')) }}

			
			  <div class="control-group">
				<label class="control-label" for="inputEmail">Email</label>
				<div class="controls">
					{{ Form::text('email',null,array('class'=>'span3')) }}
				  
				</div>
			  </div>
			  <div class="control-group">
				<label class="control-label" for="inputPassword">Password</label>
				<div class="controls">
				  {{ Form::password('password',null,array('class'=>'span3')) }}
				</div>
			  </div>
			  <div class="control-group">
				<div class="controls">
				  {{ Form::submit('SignIn',array('class'=>'btn')) }} <a href="forgetpass.html">Forget password?</a>
				</div>
			  </div>
			{{ Form::close() }}
		</div>
		</div>
	</div>	
	
</div>

@stop