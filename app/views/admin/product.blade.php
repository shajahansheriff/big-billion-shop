@extends('layouts.main')

@section('content')
<div class="row">
<div id="sidebar" class="span3">
	<ul class="nav nav-list bs-docs-sidenav" style="display: block;">											
		<li class="subMenu"><a> ELECTRONICS [230]</a>
			<ul>
			<li><a class="active" href="products.html">Cameras (100)</a></li>
			<li><a href="products.html">Computers, Tablets & laptop (30)</a></li>
			<li><a href="products.html">Mobile Phone (80)</a></li>
			<li><a href="products.html">Sound & Vision (15)</a></li>
			</ul>
		</li>
		<li class="subMenu"><a> CLOTHES [840] </a>
		<ul>
			<li><a href="products.html">Women's Clothing (45)</a></li>
			<li><a href="products.html">Women's Shoes (8)</a></li>												
			<li><a href="products.html">Women's Hand Bags (5)</a></li>	
			<li><a href="products.html">Men's Clothings  (45)</a></li>
			<li><a href="products.html">Men's Shoes (6)</a></li>												
			<li><a href="products.html">Kids Clothing (5)</a></li>												
			<li><a href="products.html">Kids Shoes (3)</a></li>												
		</ul>
		</li>
		<li class="subMenu"><a>FOOD AND BEVERAGES [1000]</a>
			<ul>
			<li><a href="products.html">Angoves  (35)</a></li>
			<li><a href="products.html">Bouchard Aine & Fils (8)</a></li>												
			<li><a href="products.html">French Rabbit (5)</a></li>	
			<li><a href="products.html">Louis Bernard  (45)</a></li>
			<li><a href="products.html">BIB Wine (Bag in Box) (8)</a></li>												
			<li><a href="products.html">Other Liquors & Wine (5)</a></li>												
			<li><a href="products.html">Garden (3)</a></li>												
			<li><a href="products.html">Khao Shong (11)</a></li>												
		</ul>
		</li>
		<li><a href="products.html">HEALTH & BEAUTY [18]</a></li>
		<li><a href="products.html">SPORTS & LEISURE [58]</a></li>
		<li><a href="products.html">BOOKS & ENTERTAINMENTS [14]</a></li>
		<li style="border:0"> &nbsp;</li>						
		<li> <a href="product_summary.html"><strong>3 Items in your cart  <span class="badge badge-warning pull-right" style="line-height:18px;">$155.00</span></strong></a></li>
		<li style="border:0"> &nbsp;</li>	
		<li>
		  <div class="thumbnail">
			<img src="assets/products/1.jpg" alt="">
			<div class="caption">
			  <h5>Product name</h5>
			  <p> 
				Lorem Ipsum is simply dummy text. 
			  </p>
			  <h4><a class="btn" href="product_details.html">VIEW</a> <span class="pull-right">$222.00</span></h4>
			</div>
		  </div>
		</li>
		<li style="border:0"> &nbsp;</li>		
		<li class="last">
		  <div class="thumbnail">
			<img src="assets/products/2.jpg" alt="">
			<div class="caption">
			  <h5>Product name</h5>
			  <p> 
				Lorem Ipsum is simply dummy text. 
			  </p>
			  <h4><a class="btn" href="product_details.html">VIEW</a> <span class="pull-right">$222.00</span></h4>
			</div>
		  </div>
		</li> 
	  </ul>
</div>

	<div class="span9">
    <ul class="breadcrumb">
		<li><a href="index.html">Home</a> <span class="divider">/</span></li>
		<li class="active">Registration</li>
    </ul>
	<h3> List Of Categories</h3>	
	<hr class="soft"/>
	<div class="well">
	
		@if (Session::has('message'))
		
			<div class="alert alert-block alert-error fade in">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{{ Session::get('message') }}
	 		</div>
		
		@endif

		@if ($errors->has()) 
		<ul>
			@foreach ($errors->all() as $error) 
			<li>
				{{$error}}
			</li>
			@endforeach
		</ul>
		@endif
		<br>
		
	<table class="table table-bordered">	
		@foreach ($products as $product) 
		 	<tr>
		 		<td> {{HTML::image($product->image,$product->title,array('width'=>'50','height'=>'50')) }}</td>
		 		<td> {{ $product->title }} </td> 
		 		<td>
		 		{{ Form::open(array('url'=>'admin/product/destroy')) }}
		 		{{ Form::hidden('product_id',$product->id) }}
		 		{{ Form::submit('Delete',array('class'=>'btn btn-mini active')) }}
		 		{{ Form::close() }}
		 		</td>
		 		<td>
		 		{{ Form::open(array('url'=>'admin/product/toggle-availabilty')) }}
		 		{{ Form::hidden('product_id',$product->id)}}
		 		{{ Form::select('availability',array('1'=>'In Stock','0'=>'Out of Stock'), $product->availability) }}
		 		{{ Form::submit('Update') }}
		 		{{ Form::close() }}
		 		</td>
		 	</tr>
		 
		@endforeach
	 </table>

	{{ Form::open(array('url'=>'admin/product/create','class'=>'form-horizontal','files'=>true)) }}

	<h3> Add Category</h3>
	<div class="control-group">
		{{ Form::label('category','Select Category', array('class'=>'control-label')) }}
		<div class="controls">
		{{ Form::select('category_id',$categories) }}
	</div>
	</div>
	<div class="control-group">
		{{ Form::label('producttitle','Product Name:', array('class'=>'control-label')) }}
		<div class="controls">
			{{ Form::text('producttitle',null,array('placeholder'=>'Product Name'))}}  
		</div>
	</div>
	<div class="control-group">
		{{ Form::label('price','Product Price:', array('class'=>'control-label')) }}
		<div class="controls">
			{{ Form::text('price',null,array('placeholder'=>'Product Price'))}}  
		</div>
	</div>
	<div class="control-group">
		{{ Form::label('description','Description:', array('class'=>'control-label')) }}
		<div class="controls">
			{{ Form::textarea('description',null,array('placeholder'=>'Product Description'))}}  
		</div>
	</div>
	<div class="control-group">
		{{ Form::label('image','Select Image:', array('class'=>'control-label')) }}
		<div class="controls">
			{{ Form::File('image')}}  
		</div>
	</div>
	<div class="control-group">
		{{ Form::label('image2','Select Image 2:', array('class'=>'control-label')) }}
		<div class="controls">
			{{ Form::File('image2')}}  
		</div>
	</div>
	<div class="control-group">
		{{ Form::label('image','Select Image 3:', array('class'=>'control-label')) }}
		<div class="controls">
			{{ Form::File('image3')}}  
		</div>
	</div>
	<div class="control-group">
		{{ Form::label('image','Select Image 4:', array('class'=>'control-label')) }}
		<div class="controls">
			{{ Form::File('image4')}}  
		</div>
	</div>
	<div class="control-group">
			<div class="controls">
				{{ Form::submit('Add',array('class'=>'btn btn-large')) }}
			</div>
		</div>
	{{ Form::close()}}
</div>

</div>
</div>
@stop