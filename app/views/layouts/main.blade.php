<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Online Shop</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	<!-- Less styles  
	<link rel="stylesheet/less" type="text/css" href="less/bootsshop.less">
	<script src="less.js" type="text/javascript"></script>
	 -->
	 
    <!-- Le styles  -->
    {{ HTML::style("assets/css/bootstrap.css") }}
    {{ HTML::style("assets/css/bootstrap-responsive.css") }}
    {{ HTML::style("assets/css/docs.css") }}
    {{ HTML::style("assets/css/style.css") }}
    {{ HTML::style("assets/js/google-code-prettify/prettify.css") }}
	

	
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="{{ asset('assets/ico/favicon.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
  </head>
<body>
 <!-- Navbar
    ================================================== -->
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
		
      <a data-target="#sidebar" data-toggle="collapse" class="btn btn-navbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="nav-collapse">
        <ul class="nav">
		  <li class="active">{{ HTML::link('/', 'Home') }}</li>
		  <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Category <b class="caret"></b></a>
              <ul class="dropdown-menu">
              	@foreach($catnav as $category)
                <li>{{ HTML::link('store/category/'.$category->id,$category->title) }}</li>
                @endforeach
              </ul>
            </li>
		  <li class=""><a href="normal.html">Delivery</a></li>
		  <li class="">{{ HTML::link('store/contact','Contact') }}</li>
		</ul>
		{{ Form::open(array('url'=>'store/search','method'=>'get','class'=>'navbar-search pull-left')) }}
		{{ Form::text('keyword',null,array('class'=>'search-query span5','placeholder'=>'Im Looking for..')) }}
		{{ Form::close() }}
        
        <ul class="nav pull-right">
		<li class="dropdown">
			
			@if(Auth::check())
				<li class="dropdown">
	              <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->fname }} <b class="caret"></b></a>
	              <ul class="dropdown-menu">
	                <li>{{ HTML::link('store/cart','Cart') }}</li>
	                @if( Auth::user()->admin == 1)
	                 <li>{{ HTML::link('admin/category','Manage Category') }}</li>
	                 <li>{{ HTML::link('admin/product','Manage Product') }}</li>
	                @endif
	                <li class="divider"></li>
	                <li>{{ HTML::link('user/signout', 'Log Out') }}</li>
	              </ul>
            	</li>

			@else
			<li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li>{{ HTML::link('user/signin','Sign In') }}</li>
                <li>{{ HTML::link('user/signup','Sign Up') }}</li>
                
              </ul>
            </li>

			@endif

			
			<div class="dropdown-menu">
			<form class="form-horizontal loginFrm">
			  <div class="control-group">								
				<input type="text" class="span2" id="inputEmail" placeholder="Email">
			  </div>
			  <div class="control-group">
				<input type="password" class="span2" id="inputPassword" placeholder="Password">
			  </div>
			  <div class="control-group">
				<label class="checkbox">
				<input type="checkbox"> Remember me
				</label>
				<button type="submit" class="btn pull-right">Sign in</button>
			  </div>
			</form>					
			</div>
		</li>
		</ul>
      </div><!-- /.nav-collapse -->
    </div>
  </div><!-- /navbar-inner -->
</div>
<!-- ======================================================================================================================== -->	
<div id="mainBody" class="container">
<header id="header">
<div class="row">
<div class="span12">
	<a href="/">{{ HTML::image("assets/img/logo.png","Bootsshop") }}
<div class="pull-right"> <br/>
	<a href="product_summary.html"> <span class="btn btn-mini btn-warning"> <i class="icon-shopping-cart icon-white"></i> {{Cart::totalItems() }} </span> </a>
	<a href="product_summary.html"><span class="btn btn-mini active">Rs. {{ Cart::total() }}</span></a>
	<span class="btn btn-mini">&pound;</span>
	<span class="btn btn-mini">&euro;</span> 
</div>
</div>
</div>
<div class="clr"></div>
</header>
<!-- ==================================================Header End====================================================================== -->
@if($page == 'index')
	@include('layouts.indexCarousel')
@endif

<div class="row">
	@if($page == 'index')
	@else @include('layouts.side_menu')
	@endif
	@yield('content')
</div>
<!-- Footer ------------------------------------------------------ -->
<hr class="soft">
<div  id="footerSection">
	<div class="row">
		<div class="span3">
			<h5>ACCOUNT</h6>
			{{ HTML::link('store/cart', 'Cart') }}
			<a href="#">PERSONAL INFORMATION</a> 
			{{ HTML::link('store/contact', 'Contact') }}
			<a href="#">DISCOUNT</a>  
			<a href="#">ORDER HISTORY</a>
		 </div>
		<div class="span3">
			<h5>INFORMATION</h5>
			{{ HTML::link('store/contact', 'Contact') }}
			{{ HTML::link('user/signup', 'SignUp') }} 
			<a href="legal_notice.html">LEGAL NOTICE</a>  
			{{ HTML::link('store/toc', 'Terms And Condition') }}
			{{ HTML::link('store/contact', 'Contact') }}
		 </div>
		<div class="span3">
			<h5>OUR OFFERS</h5>
			<a href="#">NEW PRODUCTS</a> 
			<a href="#">TOP SELLERS</a>  
			<a href="special_offer.html">SPECIAL OFFERS</a>  
			<a href="#">MANUFACTURERS</a> 
			<a href="#">SUPPLIERS</a> 
		 </div>
		<div id="socialMedia" class="span3 pull-right">
			<h5>SOCIAL MEDIA </h5>
			<a href="#">
				{{ HTML::image('assets/img/facebook.png','facebook', array('width'=>'60')) }} 
				{{ HTML::image('assets/img/twitter.png','facebook', array('width'=>'60')) }} 
				{{ HTML::image('assets/img/rss.png','facebook', array('width'=>'60')) }} 
		 </div> 
	 </div>
	 <hr class="soft">
	<p class="pull-right">&copy; Boot'sshop</p>
</div><!-- /container -->


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    {{ HTML::script("http://platform.twitter.com/widgets.js") }}
    {{ HTML::script("assets/js/jquery.js") }}
	{{ HTML::script("assets/js/google-code-prettify/prettify.js") }}
    {{ HTML::script("assets/js/application.js") }}
    {{ HTML::script("assets/js/bootstrap-transition.js") }}
    {{ HTML::script("assets/js/bootstrap-modal.js") }}
    {{ HTML::script("assets/js/bootstrap-scrollspy.js") }}
    {{ HTML::script("assets/js/bootstrap-alert.js") }}
    {{ HTML::script("assets/js/bootstrap-dropdown.js") }}
    {{ HTML::script("assets/js/bootstrap-tab.js") }}
    {{ HTML::script("assets/js/bootstrap-tooltip.js") }}
    {{ HTML::script("assets/js/bootstrap-popover.js") }}
    {{ HTML::script("assets/js/bootstrap-button.js") }}
    {{ HTML::script("assets/js/bootstrap-collapse.js") }}
    {{ HTML::script("assets/js/bootstrap-carousel.js") }}
    {{ HTML::script('assets/js/bootstrap-typeahead.js') }}
    {{ HTML::script('assets/js/bootstrap-affix.js')}}
    
   {{ HTML::script("assets/js/jquery.lightbox-0.5.js") }}
	{{ HTML::script("assets/js/bootsshoptgl.js") }}
	 <script type="text/javascript">
    $(function() {
        $('#gallery a').lightBox();
    });
    </script>
  </body>
</html>