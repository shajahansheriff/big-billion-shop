@if($page !='store.contact')
<div id="sidebar" class="span3">
	<ul class="nav nav-list bs-docs-sidenav" style="display: block;">											
		<li class="subMenu"><h4>Category</h4></li>
		@foreach($catnav as $category )
		
		<li> {{ HTML::link('store/category/'.$category->id, $category->title) }}</li>	
		
		@endforeach
		<!-- <li class="subMenu"><a> CLOTHES [840] </a>
		
		</li>
		<li class="subMenu"><a>FOOD AND BEVERAGES [1000]</a>
			<ul>
			<li><a href="products.html">Angoves  (35)</a></li>
			<li><a href="products.html">Bouchard Aine & Fils (8)</a></li>												
			<li><a href="products.html">French Rabbit (5)</a></li>	
			<li><a href="products.html">Louis Bernard  (45)</a></li>
			<li><a href="products.html">BIB Wine (Bag in Box) (8)</a></li>												
			<li><a href="products.html">Other Liquors & Wine (5)</a></li>												
			<li><a href="products.html">Garden (3)</a></li>												
			<li><a href="products.html">Khao Shong (11)</a></li>												
		</ul>
		</li>
		<li><a href="products.html">HEALTH & BEAUTY [18]</a></li>
		<li><a href="products.html">SPORTS & LEISURE [58]</a></li>
		<li><a href="products.html">BOOKS & ENTERTAINMENTS [14]</a></li> -->
							
		<li> <a href="../cart"><strong>{{ Cart::totalItems() }} Items in your cart  <span class="badge badge-warning pull-right" style="line-height:18px;">Rs. {{ Cart::total() }}</span></strong></a></li>
		@foreach(Cart::contents() as $cartProduct)
		 <li style="border:0"> &nbsp;</li>	
		<li>
		  <div class="thumbnail">
			{{ HTML::image($cartProduct->image, $cartProduct->name) }}
			<div class="caption">
			  <h5>{{ $cartProduct->name}}</h5>
			  <p> 
				{{ $cartProduct->quantity }} 
			  </p>
			  <h4>{{ HTML::link('store/product/'.$cartProduct->id,'VIEW',array('class'=>'btn'))}} <span class="pull-right">Rs. {{ $cartProduct->price }}</span></h4>
			</div>
		  </div>
		</li> 
		@endforeach
	  </ul>
</div>
@endif