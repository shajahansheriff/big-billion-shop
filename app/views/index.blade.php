@extends('layouts.main')

@section('content')

	
	
	<div class="span12">

	<div>

		<h3>New Products </h3>

		<div class="row-fluid">
			<div id="myCarousel1" class="carousel slide">
	            <div class="carousel-inner">
	              <div class="item active">
			  <ul class="thumbnails">
			
				@foreach ($new_products as $product) 
				<li class="span2">
				  <div class="thumbnail">
				  <a href="#" class="tag" src="assets/products/new.png"></a>
					<a  href="store/product/{{ $product->id }}">{{ HTML::image($product->image,$product->title) }}</a>
					<div class="caption">
					  <h5>{{ $product->title }}</h5>
					  <p>
						{{ $product->Description }} 
					  </p>
					  <h4>{{ HTML::link('store/product'.$product->id,'VIEW',array('class'=>'btn')) }} <span class="pull-right">Rs. {{ $product->price }}</span></h4>
					</div>
				  </div>
				</li>
				
				@endforeach
				
			  </ul>
			  </div>
			  </div>
			 <!--  <a class="left carousel-control" href="#myCarousel1" data-slide="prev">&lsaquo;</a>
	            <a class="right carousel-control" href="#myCarousel1" data-slide="next">&rsaquo;</a> -->
			  </div>		  
		  </div>
		  
		  <hr>
		  
		  
		  <h3>Popular Products </h3>
		  
		  <ul class="thumbnails">
			
		  	@foreach ($popular_products as $popular_product) 
			<li class="span3">
			  <div class="thumbnail">
				<a  href="store/product/{{ $popular_product->id }}">{{ HTML::image($popular_product->image,$popular_product->title) }}</a>
				<div class="caption">
				  <h5>{{ $popular_product->title }}</h5>
				  <p> 
					{{ $popular_product->Description }}
				  </p>
				  <h4>{{ HTML::link('store/product'.$popular_product->id,'VIEW',array('class'=>'btn')) }}<span class="pull-right">Rs. {{ $popular_product->price }}</span></h4>
				</div>
			  </div>
			</li>
			@endforeach
			
		  </ul>	
		</div>
	</div>
	
@stop